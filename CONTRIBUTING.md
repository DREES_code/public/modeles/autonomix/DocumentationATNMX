Les utilisateurs de cette documentation sont invités à signaler à la DREES les éventuels problèmes ou éléments manquants qu'ils rencontreraient, en écrivant à DREES_CODE@sante.gouv.fr. 

Pour les utlisateurs de Gitlab, il est possible de le faire en ouvrant une "issue" dans le projet. 

Si vous souhaitez contribuer à la documentation du modèle, il est également possible de suggérer des ajouts à la documentation via une branche dédiée et une "merge request". 

Merci de vos retours.
